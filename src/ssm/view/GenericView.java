/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.view;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;


import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import static ssm.StartupConstants.PATH_ICONS;



/**
 *
 * @author Ali
 */
public class GenericView {
    
    private String prompt;
    private Stage dialog;
    private String title;
    private HBox hBox;
    private VBox vBox;
    private Text text;
    private TextField textField;
    private String string;
    private int width;
    private int height;
    private int font;
    private String response;
    private Button ok;
    private Button cancel;
    private SlideShowMakerView ui;
    private Image icon;
    
    public  GenericView(String title, String prompt, TextField textfield, HBox hbox, VBox vbox, 
            Button b1, Button b2, SlideShowMakerView ui)
    {        this.title = title;
             this.prompt = prompt;
             hBox = hbox;
             vBox = vbox;
             textField = textfield;
             ok = b1;
             cancel = b2;
             this.ui = ui;
             icon = new Image("http://moodle.kvilleps.org/file.php/1/rocket3D.png");
             
    }
    public  GenericView(String title, String prompt,HBox hbox, VBox vbox, Button b1, Button b2, SlideShowMakerView ui)
    {   this.title = title;
             this.prompt = prompt;
             vBox = vbox;
             hBox = hbox;
             ok = b1;
             cancel = b2;
             this.ui = ui;
            // icon = new Image("http://moodle.kvilleps.org/file.php/1/rocket3D.png");
    }
    public  GenericView(String title, String prompt, HBox hbox, Button b1, Button b2, SlideShowMakerView ui)
    {   this.title = title;
             this.prompt = prompt;
             hBox = hbox;
             ok = b1;
             cancel = b2;
             this.ui = ui;
            icon = new Image("http://moodle.kvilleps.org/file.php/1/rocket3D.png");
             
    }
    
    public GenericView(String title, String prompt, VBox vbox, Button b1, Button b2, SlideShowMakerView ui)
    {       this.title = title;
            this.prompt = prompt;
            vBox = vbox; 
            ok = b1;
            cancel = b2;
            this.ui = ui;
            icon = new Image("http://moodle.kvilleps.org/file.php/1/rocket3D.png");
    }
    public GenericView(String title, String prompt, VBox vbox, Button b1,SlideShowMakerView ui)
    {   this.title = title;
             this.prompt = prompt;
             vBox = vbox;
             ok = b1;
             this.ui = ui;
           // icon = new Image("http://moodle.kvilleps.org/file.php/1/rocket3D.png");
    }
    
    public GenericView initTextFieldWindow()
    {   
        dialog = new Stage();
        dialog.setTitle(title);
        dialog.getIcons().add(icon);
        textField.setMaxWidth(200);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10);
        vBox.getChildren().add(new Text(prompt));
        vBox.getChildren().add(textField);
        hBox.setAlignment(Pos.CENTER);
        hBox.setSpacing(10);
        hBox.getChildren().add(ok);
        hBox.getChildren().add(cancel);
        vBox.getChildren().add(hBox);
        Scene scene = new Scene(vBox, 400, 200);
        dialog.setScene(scene);
        ok.setOnAction(e -> {
            response = textField.getText();
                       ui.updateToolbarControls(false);
                       dialog.close();});
        cancel.setOnAction(e -> {
                          dialog.close();});
        dialog.showAndWait();
        
        
        return this;
     }
    
     public GenericView initMessageWindow()
     {  
        dialog = new Stage();
        dialog.setTitle(title);
        //dialog.getIcons().add(icon);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10);
        vBox.getChildren().add(new Text(prompt));
        vBox.getChildren().add(ok);
        Scene scene = new Scene(vBox, 400, 200);
        dialog.setScene(scene);
        ok.setOnAction(e -> {
            response = ok.getText();
            dialog.close();});
      
           dialog.showAndWait();
        return this;
     }
     
     public GenericView initMessageChoiceWindow()
     {  
        dialog = new Stage();
        dialog.setTitle(title);
        //dialog.getIcons().add(icon);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10);
        vBox.getChildren().add(new Text(prompt));
        hBox.setAlignment(Pos.CENTER);
        hBox.setSpacing(10);
        hBox.getChildren().add(ok);
        hBox.getChildren().add(cancel);
        vBox.getChildren().add(hBox);
        Scene scene = new Scene(vBox, 400, 200);
        dialog.setScene(scene);
        ok.setOnAction(e -> {
            response = ok.getText();
            dialog.close();});
        cancel.setOnAction(e -> {
            response = cancel.getText();
            dialog.close();});
        dialog.showAndWait();
        return this;
     }
     
     public String getTextFieldData()
     {  
         return textField.getText();
     }
     
     public Button getOk()
     {  
         return ok;
         
     }
     
     public Button getCancel()
     {  
         return cancel;
         
     }
     
     public String getResponse()
     {
         
         return response;
         
     }
     
     public String getUrduString()
     {
         return cancel.getText();
     }
     
     
     
      
     
     
     
     
     
     
            
    
        
        
       
        
        
        
}
   
