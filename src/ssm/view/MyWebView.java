/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.view;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import static jdk.nashorn.internal.objects.NativeRegExp.source;
import static java.nio.file.StandardCopyOption.*;
import javafx.scene.control.Dialog;

/**
 *
 * @author Ali
 */
public class MyWebView extends Application {

    SlideShowMakerView ui;
    Path title;
    Path html;
    File titleFolder;
    WebEngine webEngine;

    public MyWebView(SlideShowMakerView ui) throws Exception {
        this.ui = ui;
        title = Paths.get("data/sites/" + ui.getSlideShow().getTitle());
        html = Paths.get(title.toString() + "/index.html");

        createDirectory();
        start(new Stage());

    }

    @Override
    public void start(Stage dialog) throws Exception {
        System.out.println(html.toString());
        dialog = new Stage();
        dialog.setTitle(ui.getSlideShow().getTitle());
        WebView browser = new WebView();
        webEngine = browser.getEngine();
        File htmlSource = new File(html.toString());
        webEngine.load(htmlSource.toURI().toURL().toString());
        Scene scene = new Scene(browser);
        dialog.setScene(scene);
        dialog.showAndWait();
        dialog.close();
 }

    public void createDirectory() throws IOException {
        Path sourceHtml = Paths.get("data/html/index.html");
        Path sourceJs = Paths.get("data/js/slideshow.js");
        Path sourceCss = Paths.get("data/css/slideshow_style.css");
        Path sourceJson = Paths.get("data/slide_shows/" + ui.getSlideShow().getTitle() + ".json");
        Path sourceJQ = Paths.get("data/js/jquery-1.11.3.js");
        Path sourceIco = Paths.get("data/icons");
        Path sourceNxt = Paths.get(sourceIco.toString() + "/Next.png");
        Path sourcePrv = Paths.get(sourceIco.toString() + "/Previous.png");
        Path sourcePla = Paths.get(sourceIco.toString() + "/Play.png");
        Path sourcePau = Paths.get(sourceIco.toString() + "/Pause.png");

        //Destination paths
        Path desJs = Paths.get(title.toString() + "/js");
        Path desCss = Paths.get(title.toString() + "/css");
        Path desImg = Paths.get(title.toString() + "/img");
        Path desIco = Paths.get(title.toString() + "/icons");
        Path jsonName = Paths.get("dataFile.json");
        Path jsonFile = Paths.get(title.toString() + "/" + jsonName.toString());

        //Creating the sites folder
        new File("data/sites").mkdirs();
        titleFolder = new File(title.toString());

        if (!titleFolder.exists()) {
            titleFolder.mkdirs();
            new File(jsonName.toString());
            Files.copy(sourceJson, title.resolve(jsonName), REPLACE_EXISTING);
            new File(desJs.toString()).mkdirs();
            Files.copy(sourceJs, desJs.resolve(sourceJs.getFileName()), REPLACE_EXISTING);
            Files.copy(sourceHtml, title.resolve(sourceHtml.getFileName()), REPLACE_EXISTING);

            Files.copy(sourceIco, title.resolve(sourceIco.getFileName()));
            Files.copy(sourceJQ, desJs.resolve(sourceJQ.getFileName()), REPLACE_EXISTING);
            new File(desCss.toString()).mkdirs();
            Files.copy(sourceCss, desCss.resolve(sourceCss.getFileName()), REPLACE_EXISTING);

            Files.copy(sourceNxt, desIco.resolve(sourceNxt.getFileName()), REPLACE_EXISTING);
            Files.copy(sourcePla, desIco.resolve(sourcePla.getFileName()), REPLACE_EXISTING);
            Files.copy(sourcePau, desIco.resolve(sourcePau.getFileName()), REPLACE_EXISTING);
            Files.copy(sourcePrv, desIco.resolve(sourcePrv.getFileName()), REPLACE_EXISTING);

            File imageFolder = new File(desImg.toString());
            imageFolder.mkdirs();
            //copying images
            int size = ui.getSlideShow().getSlides().size();
            for (int i = 0; i < size; i++) {
                Path a = Paths.get(ui.getSlideShow().getSlides().get(i).getImagePath()
                        + ui.getSlideShow().getSlides().get(i).getImageFileName());
                Files.copy(a, desImg.resolve(a.getFileName()), REPLACE_EXISTING);
            }
        } else {
            File imageFolder = new File(desImg.toString());
            Files.copy(sourceJson, title.resolve(jsonName), REPLACE_EXISTING);
            this.deleteDirectory(imageFolder);
            imageFolder.mkdirs();
            int size = ui.getSlideShow().getSlides().size();
            for (int i = 0; i < size; i++) {
                Path a = Paths.get(ui.getSlideShow().getSlides().get(i).getImagePath()
                        + ui.getSlideShow().getSlides().get(i).getImageFileName());
                Files.copy(a, desImg.resolve(a.getFileName()), REPLACE_EXISTING);
            }
        }

    }

    public void deleteDirectory(File file) throws IOException {
        if (file.isDirectory()) {
            File[] children = file.listFiles();
            for (int i = 0; i < children.length; i++) {
                System.out.println(children[i].toString());
                Path path = Paths.get(children[i].getPath());
                deleteDirectory(children[i]);
                Files.deleteIfExists(path);
                System.out.println(children[i].toString());
            }
        }
        file.delete();
    }
}
