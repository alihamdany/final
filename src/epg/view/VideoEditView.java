/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static epg.StartupConstants.ICON_OK;
import static epg.StartupConstants.ICON_REMOVE;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UIHEADER;
import static epg.StartupConstants.TOOLTIP_NEW_EPORTFOLIO;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author Ali
 */
public class VideoEditView {
    Stage dialog;
    TextField textField;
    VBox vBox;
    VBox imageContainer;
    HBox hBox;
    Button ok;
    Button cancel;
    String response;
    
    public VideoEditView(){
    
        imageContainer = new VBox();
        imageContainer.setId("videocontainer");
        imageContainer.setPrefHeight(400);
       
        dialog = new Stage();
        dialog.setTitle("Edit Video");
        //dialog.getIcons().add(icon);
        textField = new TextField();
        textField.setMaxWidth(200);
        vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10);
        vBox.getChildren().add(new Text("Please add a video and caption"));
        Media video = new Media("http://www.youtube.com/embed/e2zyjbH9zzA");
        MediaPlayer myPlayer = new MediaPlayer(video);
        MediaView videoView = new MediaView(myPlayer);
        vBox.getChildren().add(imageContainer);
        vBox.getChildren().add(textField);
        hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        hBox.setSpacing(10);
       
        vBox.getChildren().add(hBox);
        Scene scene = new Scene(vBox, 800, 600);
        scene.getStylesheets().add(STYLE_SHEET_UIHEADER);
        ok = initChildButton(hBox, ICON_OK,	  TOOLTIP_NEW_EPORTFOLIO,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        cancel = initChildButton(hBox, ICON_REMOVE,	  TOOLTIP_NEW_EPORTFOLIO,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);

        dialog.setScene(scene);
        ok.setOnAction(e -> {
            response = textField.getText();
            dialog.close();});
        cancel.setOnAction(e -> {
            dialog.close();});
        dialog.showAndWait();
    }
    
    public Button initChildButton(
            Pane toolbar,
            String iconFileName,
            String tooltip,
            String cssClass,
            boolean disabled) {
        
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(tooltip.toString());
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        //toolbar.setId("vertical_toolbar");
        return button;
    }
}
