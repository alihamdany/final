/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package epg.view;

import static epg.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static epg.StartupConstants.ICON_AddList;
import static epg.StartupConstants.ICON_OK;
import static epg.StartupConstants.ICON_REMOVE;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UIHEADER;
import static epg.StartupConstants.TOOLTIP_NEW_EPORTFOLIO;
import epg.model.List;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author Ali
 */
public class ListEditView {
    Stage dialog;
    TextField textField1;
    TextField textField2;
    TextField textField3;
    TextField textField4;
    TextField textField5;
    VBox vBox;
    HBox radioButtons;
    HBox okButtons;
    Button ok;
    Button cancel;
    RadioButton text;
    RadioButton hyperlink;
    String response;
    Button removeButton;
    Button addButton;
    private PageUI pageui;
    private MainUI ui;
    private List list;
    
    public ListEditView(PageUI spageui, MainUI sui){
        this.pageui = spageui;
        this.ui = sui;
        list = new List();

    }
    
    public Button initChildButton(
            Pane toolbar,
            String iconFileName,
            String tooltip,
            String cssClass,
            boolean disabled) {
        
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(tooltip.toString());
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        //toolbar.setId("vertical_toolbar");
        return button;
    }
    
    public void initWIndow()
    {
        

        dialog = new Stage();
        dialog.setTitle("Edit List");
        vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10);
        //vBox.getChildren().add(new Text("What would you like in your List?"));
        
        //hbox1.setSpacing(10);
        addButton = initChildButton(vBox, ICON_AddList,	  "Add",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        initEventHandlers();
        okButtons = new HBox();
        okButtons.setAlignment(Pos.CENTER);
        okButtons.setSpacing(10);
        Scene scene = new Scene(vBox, 800, 600);
        scene.getStylesheets().add(STYLE_SHEET_UIHEADER);
        vBox.getChildren().addAll(new Text("What would you like in your List?"));
        //addTextField();
        vBox.getChildren().add(okButtons);
        ok = initChildButton(okButtons, ICON_OK,	  "OK",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        cancel = initChildButton(okButtons, ICON_REMOVE,	  "Cancel",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        dialog.setScene(scene);
        ok.setOnAction(e -> {
            pageui.addList(list);
            dialog.close();});
        cancel.setOnAction(e -> {
            dialog.close();});
        dialog.showAndWait();

    }
    
    public void addTextField()
    {
        HBox hbox = new HBox();
        hbox.setAlignment(Pos.CENTER);
        TextField textField = new TextField();
        textField.setMaxWidth(200);
        textField.setOnAction(e->{list.addItem(textField.getText());
        
        System.out.println(list.getList().size());});
        //addButton = initChildButton(hbox1, ICON_OK,	  TOOLTIP_NEW_EPORTFOLIO,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        hbox.getChildren().add(textField);
        removeButton = initChildButton(hbox, ICON_REMOVE,	  "Remove",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeButton.setOnAction(e->{hbox.getChildren().clear();});
        vBox.getChildren().add(hbox);

    }
    
    public void initEventHandlers()
    {
        addButton.setOnAction(e->{addTextField();
        refresh();});
    }
    
    public void refresh()
    {
        
    }
    
    
}
