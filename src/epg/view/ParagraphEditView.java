/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static epg.StartupConstants.ICON_OK;
import static epg.StartupConstants.ICON_REMOVE;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UIHEADER;
import static epg.StartupConstants.TOOLTIP_NEW_EPORTFOLIO;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Pos;
import static javafx.geometry.Pos.TOP_LEFT;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author Ali
 */
public class ParagraphEditView {
    
    Stage dialog;
    TextArea textField;
    VBox vBox;
    HBox hBox;
    Button ok;
    Button cancel;
    String response;
     RadioButton hyperlink;
     RadioButton text;
     HBox radioButtons;
     private PageUI pageui;
    private MainUI ui;
    private String edit;
    
    public ParagraphEditView(PageUI pageui, MainUI ui, String sedit){
        this.pageui = pageui;
        this.ui = ui;
        edit = sedit;
        textField = new TextArea();
        textField.setWrapText(true);
        textField.setPrefHeight(400);
        textField.setMaxWidth(600);
        
    }
    public Button initChildButton(
            Pane toolbar,
            String iconFileName,
            String tooltip,
            String cssClass,
            boolean disabled) {
        
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(tooltip.toString());
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        //toolbar.setId("vertical_toolbar");
        return button;
    }
    
    public void initWindow()
    {
        dialog = new Stage();
        dialog.setTitle("Edit Paragraph");
        //dialog.getIcons().add(icon);
        
        //textField.setAlignment(TOP_LEFT);
        
        ToggleGroup groupOfRadio = new ToggleGroup();
        text = new RadioButton("Text"); 
        text.setToggleGroup(groupOfRadio);
        hyperlink = new RadioButton("Hyperlink");
        hyperlink.setToggleGroup(groupOfRadio);

        radioButtons = new HBox();
        radioButtons.getChildren().addAll(text,hyperlink);
        radioButtons.setAlignment(Pos.CENTER);

        
        vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10);
        vBox.getChildren().add(new Text("What would you like in your Paragraph?"));
        vBox.getChildren().add(textField);
        vBox.getChildren().add(radioButtons);
        hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        hBox.setSpacing(10);
        
        vBox.getChildren().add(hBox);
        Scene scene = new Scene(vBox, 800, 600);
        scene.getStylesheets().add(STYLE_SHEET_UIHEADER);
        ok = initChildButton(hBox, ICON_OK,	  TOOLTIP_NEW_EPORTFOLIO,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        cancel = initChildButton(hBox, ICON_REMOVE,	  TOOLTIP_NEW_EPORTFOLIO,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        dialog.setScene(scene);
        if(!edit.equals("edit")){
            ok.setOnAction(e -> {
                pageui.addParagraph(textField.getText());
                //vBox.getChildren().add(hBox);
                dialog.close();});}
        else
        {
            ok.setOnAction(e -> {
                try {
                    pageui.editParagraph(textField.getText());
                } catch (MalformedURLException ex) {
                    Logger.getLogger(ParagraphEditView.class.getName()).log(Level.SEVERE, null, ex);
                }
                //vBox.getChildren().add(hBox);
                dialog.close();});
        }
        cancel.setOnAction(e -> {
            dialog.close();});
        dialog.showAndWait();
    }
    
    public void editTextField(Text text)
    {
        textField.setText(text.getText());
    }
    
}
