/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package epg.view;

import static epg.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static epg.StartupConstants.EDIT_VIEW;
import static epg.StartupConstants.ICON_CHANGE_COLOR;
import static epg.StartupConstants.ICON_CHANGE_FONT;
import static epg.StartupConstants.ICON_EDIT;
import static epg.StartupConstants.ICON_INSERT_LIST;
import static epg.StartupConstants.ICON_INSERT_PARAGRAPH;
import static epg.StartupConstants.ICON_INSERT_PHOTO;
import static epg.StartupConstants.ICON_INSERT_SLIDESHOW;
import static epg.StartupConstants.ICON_INSERT_VIDEO;
import static epg.StartupConstants.ICON_REMOVE;
import static epg.StartupConstants.ICON_VIEW;
import static epg.StartupConstants.LIVE_VIEW;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.TOOLTIP_NEW_EPORTFOLIO;
import epg.controller.ComponentController;
import epg.controller.FileController;
import epg.controller.PageEditController;
import epg.file.FileManager;
import epg.model.Header;
import epg.model.List;
import epg.model.MyImage;
import epg.model.Page;
import epg.model.PageModel;
import epg.model.Paragraph;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 *
 * @author Ali
 */
public class PageUI {
    
    //Switchview toolbar
    HBox switchViewToolbarContainer;
    FlowPane switchViewToolbarPane;
    
    //Switchview toolbar buttons
    Button editViewButton;
    Button liveViewButton;
    
    //ADD Remove page toolbar
    VBox pageToolbarContainer;
    FlowPane pageToolbarPane;
    
    //ADD Remove page toolbar buttons
    Button addPageButton;
    Button removePageButton;
    
    //Component toolbar
    HBox addComponentToolbarContainer;
    FlowPane addComponentToolbarPane;
    
    //Component toolbar buttons
    Button addHeadingButton;
    Button addParagraphButton;
    Button addListButton;
    Button addImageeButton;
    Button addVideoButton;
    Button removeSlideshowButton;
    
    //ADD and Remove and Edit component buttons
    Button colorButton;
    Button fontButton;
    Button editHeaderButton;
    Button editListButton;
    Button editImageButton;
    Button editVideoButton;
    Button editSlideShowButton;
    Button editParagraphButton;
    
    Button removeHeaderButton;
    Button removeListButton;
    Button removeImageButton;
    Button removeVideoButton;
    Button removeSlideShowButton;
    Button removeParagraphButton;
    
    //Textfields
    TextField  titleField;
    
    
    //COntroller
    FileController fileController;
    FileManager fileManager;
    PageEditController pageEditController;
    ComponentController componentController;
    
    //Content Area
    VBox contentArea;
    
    //Content
    VBox content;
    
    //Tab panes
    // TabPane tabPane;
    
    //Paths
    Path html;
    
    //Containers
    HBox imageContainer;
    HBox videoContainer;
    HBox slideShowContainer;
    HBox paragraphContainer;
    HBox listContainer;
    HBox nameContainer;
    HBox titleContainer;
    HBox headerContainer;
    VBox headerContentBox;
    HBox layoutButtonContainer;
    
    //Page
    Page page;
    
    //Data
    String title;
    String name;
    Text headerText;
    Text paragraphText;
    List list;
    Text imageCaption;
    
    
    //counters 
   int headerIndex;
   int paragraphIndex;
    
    
    //Page Model
    // PageModel pages;
    
    private MainUI ui;
    private PageModel pages;
    
    public PageUI(MainUI theui, PageModel spages) throws MalformedURLException{
        ui = theui;
        this.pages = spages;
        //this.page = page;
        
        initswitchViewToolbar();
        initContentArea();
        //initButtons();
        initEventHandlers();
    }
    
    public void initContentArea() throws MalformedURLException{
        contentArea = new VBox();
        //contentArea.setAlignment(Pos.CENTER);
        //contentArea.setMaxWidth(500);
        
        contentArea.setId("contentArea");
        content = new VBox();
        
        content.setId("content");
        content.setAlignment(Pos.CENTER);
        
        
        TabPane switchPane = new TabPane();
        
        switchPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        switchPane.setId("switchpane");
        
        Tab editTab = new Tab("Edit");
        editTab.setId("editTab");
        ScrollPane scrollPane = new ScrollPane(content);
        scrollPane.setId("scrollpane");
        
        editTab.setContent(scrollPane);
        
        Tab liveTab = new Tab("Live");
        liveTab.setId("liveTab");
        liveTab.setContent(createWebview());
        VBox.setVgrow(switchPane, Priority.ALWAYS);
        switchPane.getTabs().addAll(editTab,liveTab);
        contentArea.getChildren().add(switchPane);
        titleContainer = new HBox();
        
        
        
        titleContainer.setId("titlecontainer");
        
        Label titlePrompt = new Label("Title: ");
        titlePrompt.setId("titleprompt");
        title = "Untitled";
        
        titleField = new TextField();
        titleField.setId("titlefield");
        
        titleContainer.getChildren().addAll(titlePrompt,titleField);
        content.getChildren().add(titleContainer);
        
        nameContainer = new HBox();
        nameContainer.setId("namecontainer");
        
        Label namePrompt = new Label("Student Name: ");
        namePrompt.setId("nameprompt");
        
        TextField nameField = new TextField();
        nameField.setId("namefield");
        name = nameField.getText();
        nameContainer.getChildren().addAll(namePrompt,nameField);
        content.getChildren().add(nameContainer);
        contentArea.getChildren().add(content);
        
        
        
        /* headerContainer = new HBox();
        headerContainer.setId("headercontainer");
        Text headerText = new Text();
        headerContainer.getChildren().add(headerText);
        removeHeaderButton = initChildButton(headerContainer, ICON_REMOVE,	  "Remove Header",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        //colorButton = initChildButton(headerContainer, ICON_CHANGE_COLOR,	  "Choose Color",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        //fontButton = initChildButton(headerContainer, ICON_CHANGE_FONT,	  "Choose font",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        
        
        
        
        
        
        content.getChildren().add(headerContainer);*/
        
               
        
    }
    
    private void initEventHandlers(){
        componentController = new ComponentController(ui,this);
        // pageEditController = new PageEditController(ui,this);
        
        
        
        
        
        /*
        
        
        editHeaderButton.setOnAction(e -> {
        componentController.handleEditHeaderRequest(headerText);
        });
        
        /*removeListButton.setOnAction(e -> {
        componentController.handleRemoveRequest("list",listContainer);
        });
        
        removeImageButton.setOnAction(e -> {
        componentController.handleRemoveRequest("image",imageContainer);
        });
        
        removeVideoButton.setOnAction(e -> {
        componentController.handleRemoveRequest("video",videoContainer);
        });
        
        removeSlideShowButton.setOnAction(e -> {
        componentController.handleRemoveRequest("slideShow",slideShowContainer);
        });
        
        removeParagraphButton.setOnAction(e -> {
        componentController.handleRemoveRequest("paragraph",paragraphContainer);
        });
        
        editParagraphButton.setOnAction(e -> {
        componentController.handleEditParagraphRequest(paragraphText);
        });*/
        
        titleField.setOnAction(e->{
            
            title = titleField.getText();
            //page.setTitle(title);
            ui.pages.getSelectedPage().setTitle(title);
            System.out.println("Title: " + ui.pages.getSelectedPage().getTitle());
            try {
                ui.reloadPage() ;
            } catch (MalformedURLException ex) {
                Logger.getLogger(PageUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        });
        
    }
    
    public Button initChildButton(
            Pane toolbar,
            String iconFileName,
            String tooltip,
            String cssClass,
            boolean disabled) {
        
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(tooltip.toString());
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        //toolbar.setId("vertical_toolbar");
        return button;
    }
    
    public VBox getContentArea(){
        return contentArea;
    }
    
    public WebView createWebview() throws MalformedURLException{
        WebView browser = new WebView();
        WebEngine webEngine = browser.getEngine();
        html = Paths.get("./html/travel.html");
        File htmlSource = new File(html.toString());
        webEngine.load(htmlSource.toURI().toURL().toString());
        return browser;
    }
    
    //Toolabr for switching views
    private void initswitchViewToolbar() {
        switchViewToolbarPane = new FlowPane();
        switchViewToolbarPane.setId("switchviewtoolbarpane");
        switchViewToolbarContainer = new HBox();
        switchViewToolbarContainer.setId("switchViewToolbarContainer");
        switchViewToolbarContainer.getChildren().add(switchViewToolbarPane);
        
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        
        editViewButton = initChildButton(switchViewToolbarPane, ICON_EDIT,	  EDIT_VIEW,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        liveViewButton = initChildButton(switchViewToolbarPane, ICON_VIEW,	  LIVE_VIEW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    }
    
    private void initAddComponentToolbar() {
        addComponentToolbarPane = new FlowPane();
        //addComponentToolbarPane.setId("flow_pane");
        addComponentToolbarContainer = new HBox();
        addComponentToolbarContainer.getChildren().add(addComponentToolbarPane);
        
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        
        editViewButton = initChildButton(addComponentToolbarPane, ICON_EDIT,	  EDIT_VIEW,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        liveViewButton = initChildButton(addComponentToolbarPane, ICON_VIEW,	  LIVE_VIEW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        editViewButton = initChildButton(addComponentToolbarPane, ICON_EDIT,	  EDIT_VIEW,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        liveViewButton = initChildButton(addComponentToolbarPane, ICON_VIEW,	  LIVE_VIEW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        editViewButton = initChildButton(addComponentToolbarPane, ICON_EDIT,	  EDIT_VIEW,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        liveViewButton = initChildButton(addComponentToolbarPane, ICON_VIEW,	  LIVE_VIEW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    }
    
    public void addHeader(String text) throws MalformedURLException
    {
        TextField editTextField = new TextField();

        HBox genericBox = new HBox();
        genericBox.setAlignment(Pos.CENTER);
        //genericBox.setMaxWidth(50);
        Header header = new Header(text);
        pages.getSelectedPage().addHeader(header);
        pages.getSelectedPage().addComponent("header");
        headerText = new Text(text);
       
        headerIndex = pages.getSelectedPage().getHeader().lastIndexOf(header);
        genericBox.getChildren().addAll(new Text(pages.getSelectedPage().getHeader().get(headerIndex).getHeaderText()));
        editHeaderButton = initChildButton(genericBox, ICON_EDIT,	  "Edit Header",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeHeaderButton = initChildButton(genericBox, ICON_REMOVE,	  "Remove Header",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeHeaderButton.setOnAction(e -> {
        componentController.handleRemoveRequest("header",genericBox);
        pages.getSelectedPage().removeHeader(header);
        pages.getSelectedPage().removeComponent("header");
        });
        editHeaderButton.setOnAction(e -> {
        genericBox.getChildren().add(editTextField);
        //componentController.handleEditHeaderRequest(headerText);
        });
        editTextField.setOnAction(e->{headerText = new Text(editTextField.getText());
        pages.getSelectedPage().getHeader().get(headerIndex).setHeaderText(headerText.getText());
            try {
                ui.reloadPage();
            } catch (MalformedURLException ex) {
                Logger.getLogger(PageUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        
        content.getChildren().add(genericBox);
    }

    
    public void loadHeader(Header header)
    {
        TextField editTextField = new TextField();
        HBox genericBox = new HBox();
        genericBox.setAlignment(Pos.CENTER);
        headerText = new Text(header.getHeaderText());
        
        genericBox.getChildren().addAll(headerText);
        editHeaderButton = initChildButton(genericBox, ICON_EDIT,	  "Edit Header",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeHeaderButton = initChildButton(genericBox, ICON_REMOVE,	  "Remove Header",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeHeaderButton.setOnAction(e -> {
        componentController.handleRemoveRequest("header",genericBox);
        pages.getSelectedPage().removeHeader(header);
        pages.getSelectedPage().removeComponent("header");
        });
         editHeaderButton.setOnAction(e -> {
        genericBox.getChildren().add(editTextField);
        //componentController.handleEditHeaderRequest(headerText);
        });
        editTextField.setOnAction(e->{headerText = new Text(editTextField.getText());
        pages.getSelectedPage().getHeader().get(headerIndex).setHeaderText(headerText.getText());
            try {
                ui.reloadPage();
            } catch (MalformedURLException ex) {
                Logger.getLogger(PageUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        content.getChildren().add(genericBox);
        
    }
    
    public int reloadHeader(int n)
    {
        int i =n;
        System.out.println("header list size: " + pages.getSelectedPage().getHeader().size());
        System.out.println("header list size: " + pages.getSelectedPage().getComponentList().size());
        if((pages.getSelectedPage().getHeader().size())>0)
        {   
            //i = pages.getSelectedPage().getParagraph().listIterator().nextIndex();
            loadHeader(pages.getSelectedPage().getHeader().get(i));
        }
        return i;
    }
        
        
        
        
        
        
    
    
    public void initButtons()
    {
        
        headerContainer = new HBox();
        headerContainer.setId("headercontainer");
        //removeHeaderButton = initChildButton(headerContainer, ICON_REMOVE,	  "Remove Header",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        //editHeaderButton = initChildButton(headerContainer, ICON_EDIT,	  "Edit Header",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        // removeParagraphButton = initChildButton(paragraphContainer, ICON_REMOVE,	  "Remove Paragraph",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        //editParagraphButton = initChildButton(paragraphContainer, ICON_EDIT,	  "Edit Paragraph",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeListButton = initChildButton(paragraphContainer, ICON_REMOVE,	  "Remove List",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        editListButton = initChildButton(paragraphContainer, ICON_EDIT,	  "Edit List",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        
    }
    
    public void addParagraph(String text)
    {
        HBox genericBox = new HBox();
        genericBox.setMaxWidth(50);
        
        genericBox.setAlignment(Pos.CENTER);

        
        
        
        
        Paragraph paragraph = new Paragraph(text);
        pages.getSelectedPage().addParagraph(paragraph);
        pages.getSelectedPage().addComponent("paragraph");
        paragraphText = new Text(text);
        
        
        paragraphIndex = pages.getSelectedPage().getParagraph().lastIndexOf(paragraph);
        genericBox.getChildren().addAll(new Text(pages.getSelectedPage().getParagraph().get(paragraphIndex).getParagraphText()));
        editParagraphButton = initChildButton(genericBox, ICON_EDIT,	  "Edit Paragraph",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeParagraphButton = initChildButton(genericBox, ICON_REMOVE,	  "Remove Paragraph",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        editParagraphButton.setOnAction(e -> {
        componentController.handleEditParagraphRequest(paragraphText);});
        removeParagraphButton.setOnAction(e -> {
        componentController.handleRemoveRequest("paragraph",genericBox);
        pages.getSelectedPage().removeParagraph(paragraph);
        pages.getSelectedPage().removeComponent("paragraph");
        });
        
        content.getChildren().add(genericBox);
        
    }
    
    public void loadParagraph(Paragraph paragraph)
    {
        HBox genericBox = new HBox();
        genericBox.setAlignment(Pos.CENTER);
        genericBox.setMaxWidth(50);
        //this.paragraph = paragraph;
        paragraphText = new Text(paragraph.getParagraphText());
        genericBox.getChildren().addAll(paragraphText);
        editParagraphButton = initChildButton(genericBox, ICON_EDIT,	  "Edit Paragraph",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeParagraphButton = initChildButton(genericBox, ICON_REMOVE,	  "Remove Paragraph",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        editParagraphButton.setOnAction(e -> {
        componentController.handleEditParagraphRequest(paragraphText);});
        removeParagraphButton.setOnAction(e -> {
        componentController.handleRemoveRequest("paragraph",genericBox);
        pages.getSelectedPage().removeParagraph(paragraph);
        pages.getSelectedPage().removeComponent("paragraph");
        });
        content.getChildren().add(genericBox);
    }
    
    public void editParagraph(String text) throws MalformedURLException
    {
        //int editparagraphIndex = pages.getSelectedPage().getParagraph().lastIndexOf(paragraph);

        pages.getSelectedPage().getParagraph().get(paragraphIndex).setParagraphText(text);
        ui.reloadPage();
    }
    
    public int reloadParagraph(int n) throws MalformedURLException
    {
        int i = n;
        if((pages.getSelectedPage().getParagraph().size())>0)
        {
            //i = pages.getSelectedPage().getParagraph().listIterator().nextIndex();
            System.out.println("header list size: " + pages.getSelectedPage().getHeader().size());
            System.out.println("component list size: " + pages.getSelectedPage().getComponentList().size());
            loadParagraph(pages.getSelectedPage().getParagraph().get(i));
            
        }
        return i;
    }
    
    public void addList(List list)
    {
        this.list =list;
        HBox genericHBox = new HBox();
        genericHBox.setAlignment(Pos.CENTER);
        VBox genericVBox = new VBox();
        genericHBox.getChildren().add(genericVBox);
        
        pages.getSelectedPage().addList(list);
        pages.getSelectedPage().addComponent("list");
        int listIndex;
        listIndex = pages.getSelectedPage().getList().lastIndexOf(list);
        for(int i=0; i<pages.getSelectedPage().getList().get(listIndex).getList().size(); i++)
        genericVBox.getChildren().addAll(new Text(pages.getSelectedPage().getList().get(listIndex).getList().get(i)));
        editListButton = initChildButton(genericHBox, ICON_EDIT,	  "Edit List",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeListButton = initChildButton(genericHBox, ICON_REMOVE,	  "Remove List",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeListButton.setOnAction(e -> {
        componentController.handleRemoveRequest("list",genericHBox);
        pages.getSelectedPage().removeList(list);
        pages.getSelectedPage().removeComponent("list");
        });      
        content.getChildren().add(genericHBox);
    }
    
    public int loadList(int n)
    {
        HBox genericHBox = new HBox();
        genericHBox.setAlignment(Pos.CENTER);
        VBox genericVBox = new VBox();
        genericHBox.getChildren().add(genericVBox);
        
        int i =n;
        
        //for(int i=0; i<pages.getSelectedPage().getList().size(); i++){
            for(int k =0; k<pages.getSelectedPage().getList().get(n).getList().size(); k++){
            genericVBox.getChildren().addAll(new Text(pages.getSelectedPage().getList().get(n).getList().get(k)));
        //}
        }
        editListButton = initChildButton(genericHBox, ICON_EDIT,	  "Edit List",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeListButton = initChildButton(genericHBox, ICON_REMOVE,	  "Remove List",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeListButton.setOnAction(e -> {
        componentController.handleRemoveRequest("list",genericHBox);
        pages.getSelectedPage().removeList(list);
        pages.getSelectedPage().removeComponent("list");
        });  
        content.getChildren().add(genericHBox);
        return i;
    }
    
     public int reloadList(int n) throws MalformedURLException
    {
       int i = n;
        if((pages.getSelectedPage().getList().size())>0)
        {
            //i = pages.getSelectedPage().getParagraph().listIterator().nextIndex();
            loadList(i);
            
        }
        return i;
    }
   
   public void editList()
   {
       
   }
     
     public void addImage(String caption, File imageFile) throws MalformedURLException
     {
         
         HBox genericImageBox = new HBox();
         genericImageBox.setAlignment(Pos.CENTER);
         HBox captionBox = new HBox();
         HBox dimensionBox = new HBox();
         VBox imageOrganizer = new VBox();
         MyImage image = new MyImage(imageFile, caption);
         Label captionLabel = new Label("Caption: ");
         Label height = new Label("Height: ");
         Label width = new Label("Width: ");
         TextField heightField = new TextField();
         heightField.setPrefWidth(100);
         TextField widthField = new TextField();
         widthField.setPrefWidth(100);
         dimensionBox.setPrefWidth(400);
         dimensionBox.getChildren().addAll(height,heightField,width,widthField);
        pages.getSelectedPage().addImage(image);
        pages.getSelectedPage().addComponent("image");
        
        
        int imageIndex;
        imageIndex = pages.getSelectedPage().getImage().lastIndexOf(image);
        //genericImageBox.getChildren().addAll(new Text(pages.getSelectedPage().getImage().get(imageIndex).getImageFile()));
        URL fileURL = pages.getSelectedPage().getImage().get(imageIndex).getImageFile().toURI().toURL(); 
        imageCaption = new Text(pages.getSelectedPage().getImage().get(imageIndex).getCaption());
        captionBox.getChildren().addAll(captionLabel,imageCaption);
        captionBox.setAlignment(Pos.CENTER);
        ImageView imageView;
         imageView = new ImageView(new Image(fileURL.toExternalForm()));
        imageView.setFitWidth(600);
        imageView.setPreserveRatio(true);
        imageOrganizer.getChildren().addAll(imageView,captionBox,dimensionBox);
        genericImageBox.getChildren().add(imageOrganizer);
        editListButton = initChildButton(genericImageBox, ICON_EDIT,	  "Edit List",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeListButton = initChildButton(genericImageBox, ICON_REMOVE,	  "Remove List",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeListButton.setOnAction(e -> {
        componentController.handleRemoveRequest("image",genericImageBox);
        pages.getSelectedPage().removeList(list);
        pages.getSelectedPage().removeComponent("image");
        });     
        heightField.setOnAction(e->{pages.getSelectedPage().getImage().get(imageIndex).setHeight(Integer.parseInt(heightField.getText()));});
        widthField.setOnAction(e->{pages.getSelectedPage().getImage().get(imageIndex).setWidth(Integer.parseInt(widthField.getText()));});
        content.getChildren().add(genericImageBox);
        
     }
     
     public void loadImage(MyImage image) throws MalformedURLException{
         HBox genericImageBox = new HBox();
         genericImageBox.setAlignment(Pos.CENTER);
         HBox captionBox = new HBox();
         HBox dimensionBox = new HBox();
         VBox imageOrganizer = new VBox();
         
         Label captionLabel = new Label("Caption: ");
         Label height = new Label("Height: ");
         Label width = new Label("Width: ");
         TextField heightField = new TextField();
         heightField.setPrefWidth(100);
         TextField widthField = new TextField();
         widthField.setPrefWidth(100);
         dimensionBox.setPrefWidth(400);
         heightField.setText(Double.toString(image.getHeight()));
         widthField.setText(Double.toString(image.getHeight()));
         dimensionBox.getChildren().addAll(height,heightField,width,widthField);

        int imageIndex;
        imageIndex = pages.getSelectedPage().getImage().lastIndexOf(image);
        //genericImageBox.getChildren().addAll(new Text(pages.getSelectedPage().getImage().get(imageIndex).getImageFile()));
        URL fileURL = image.getImageFile().toURI().toURL(); 
        imageCaption = new Text(image.getCaption());
        captionBox.getChildren().addAll(captionLabel,imageCaption);
        captionBox.setAlignment(Pos.CENTER);
        ImageView imageView;
         imageView = new ImageView(new Image(fileURL.toExternalForm()));
        imageView.setFitWidth(600);
        imageView.setPreserveRatio(true);
        imageOrganizer.getChildren().addAll(imageView,captionBox,dimensionBox);
        genericImageBox.getChildren().add(imageOrganizer);
        editListButton = initChildButton(genericImageBox, ICON_EDIT,	  "Edit List",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeListButton = initChildButton(genericImageBox, ICON_REMOVE,	  "Remove List",	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeListButton.setOnAction(e -> {
        componentController.handleRemoveRequest("image",genericImageBox);
        pages.getSelectedPage().removeList(list);
        pages.getSelectedPage().removeComponent("image");
        });
        heightField.setOnAction(e->{pages.getSelectedPage().getImage().get(imageIndex).setHeight(Integer.parseInt(heightField.getText()));});
        widthField.setOnAction(e->{pages.getSelectedPage().getImage().get(imageIndex).setWidth(Integer.parseInt(widthField.getText()));});
        content.getChildren().add(genericImageBox);
     }
     
     public int reloadImage(int n) throws MalformedURLException
     {
         int i =n;
        if((pages.getSelectedPage().getImage().size())>0)
        {   
            //i = pages.getSelectedPage().getParagraph().listIterator().nextIndex();
            loadImage(pages.getSelectedPage().getImage().get(i));
        }
        return i;
     }
    
 }
