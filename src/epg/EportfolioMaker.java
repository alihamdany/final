package epg;

import java.io.File;
import java.net.URL;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import static epg.StartupConstants.FINNISH_LANG;
import static epg.StartupConstants.ICON_WINDOW_LOGO;
import static epg.StartupConstants.PATH_DATA;
import static epg.StartupConstants.PATH_IMAGES;
import static epg.StartupConstants.PROPERTIES_SCHEMA_FILE_NAME;
import epg.error.ErrorHandler;
import epg.file.FileManager;
import epg.view.MainUI;
import java.net.MalformedURLException;


/**
 * SlideShowMaker is a program for making custom image slideshows. It will allow
 * the user to name their slideshow, select images to use, select captions for
 * the images, and the order of appearance for slides.
 *
 * @author McKilla Gorilla & _____________
 */
public class EportfolioMaker extends Application {
    // THIS WILL PERFORM SLIDESHOW READING AND WRITING
    FileManager fileManager = new FileManager();
    MainUI ui;

    public EportfolioMaker() throws MalformedURLException {
        this.ui = new MainUI(fileManager);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
	
	// SET THE WINDOW ICON
	String imagePath = PATH_IMAGES + ICON_WINDOW_LOGO;
	File file = new File(imagePath);
	
	// GET AND SET THE IMAGE
	URL fileURL = file.toURI().toURL();
	Image windowIcon = new Image(fileURL.toExternalForm());
	primaryStage.getIcons().add(windowIcon);
	
        // LOAD APP SETTINGS INTO THE GUI AND START IT UP
        
            

	    // NOW START THE UI IN EVENT HANDLING MODE
	    ui.startUI(primaryStage, "Enter Title");
	
	
    }
    
   
   

    /**
     * This is where the application starts execution. We'll load the
     * application properties and then use them to build our user interface and
     * start the window in event handling mode. Once in that mode, all code
     * execution will happen in response to user requests.
     *
     * @param args This application does not use any command line arguments.
     */
    public static void main(String[] args) {
	launch(args);
    }
}
