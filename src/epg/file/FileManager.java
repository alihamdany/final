package epg.file;

import static epg.StartupConstants.PATH_PAGES;
import epg.model.Page;
import epg.model.PageModel;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import static ssm.StartupConstants.PATH_SLIDE_SHOWS;
import ssm.model.Slide;
import ssm.model.SlideShowModel;

/**
 * This class uses the JSON standard to read and write slideshow data files.
 *
 * @author McKilla Gorilla & _____________
 */
public class FileManager {

    // JSON FILE READING AND WRITING CONSTANTS

    
}
