/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


/**
 *
 * @author Ali
 */
public class Page {
    
    String title;
    String name;
    ObservableList<MyImage> images;
    ObservableList<Header> headers;
    ObservableList<Paragraph> paragraphs;
    ObservableList<List> lists;
    ObservableList<SlideShow> slideshows;
    ObservableList<Video> videos;
    ObservableList<String> components;
    
    
    public Page()
    {
        title = "Untitied";
        images= FXCollections.observableArrayList();
        headers= FXCollections.observableArrayList();
        paragraphs= FXCollections.observableArrayList();
        lists= FXCollections.observableArrayList();
        slideshows= FXCollections.observableArrayList();
        videos= FXCollections.observableArrayList();
        components= FXCollections.observableArrayList();

    }
    
    public String getTitle()
    {
        return title;
    }
    
    public void setTitle(String sTitle)
    {
        title = sTitle;
    }
    
    
    public void addImage(MyImage image)
    {
        images.add(image);
        
    }
    
    public void removeImage(MyImage image)
    {
        images.remove(image);
    }
    
    public ObservableList<MyImage> getImage()
    {
        return images;
    }
    
    public void addParagraph(Paragraph paragraph)
    {
        paragraphs.add(paragraph);
        
    }
    
    public ObservableList<Paragraph> getParagraph()
    {
        return paragraphs;
    }
    
    public void removeParagraph(Paragraph paragraph)
    {
        paragraphs.remove(paragraph);
    }
    
    public void addVideo(Video video)
    {
        videos.add(video);
        
    }
    
    public void removeVideo(Video video)
    {
        videos.remove(video);
    }
    
    public ObservableList<Video> getVideo()
    {
        return videos;
    }
    
    public void addSlideshow(SlideShow slideshow)
    {
        slideshows.add(slideshow);
        
    }
    
    public void removeSlideshow(SlideShow slideshow)
    {
        slideshows.remove(slideshow);
    }
    
    public void addList(List list)
    {
        lists.add(list);
        
    }
    
    public void removeList(List list)
    {
        lists.remove(list);
    }
    
    public ObservableList<List> getList()
    {
        return lists;
    }
    
    public void addHeader(Header header)
    {
        headers.add(header);
        
    }
    
    public ObservableList<Header> getHeader()
    {
        return headers;
    }
    
    public void removeHeader(Header header)
    {
        headers.remove(header);
    }
    
    public void addComponent(String component)
    {
        components.add(component);
    }
    
    public void removeComponent(String component)
    {
        components.remove(component);
    }
    
    
    
    public ObservableList<String> getComponentList()
    {
        return components;
    }
    
    
    
    
}
