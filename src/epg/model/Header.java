/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

/**
 *
 * @author Ali
 */
public class Header {
    
    private String header;
    
    public Header(String header)
    {
        this.header = header;
    }
    
    public String getHeaderText(){
        return header;
    }
    
    public void setHeaderText(String sHeader ){
        this.header = sHeader;
    }
    
    
    
}
