/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import java.io.File;

/**
 *
 * @author Ali
 */
public class MyImage {
    
    String imageName;
    String imagePath;
    String caption;
    double width;
    double height;
    File imageFile;
    
    public MyImage(File file, String cap ){
            imageFile = file;
            caption = cap;
           
        }
    
    public String getImageName()
    {
        return imageName;
    }
    
    public void setImageName(String sImageName)
    {
        imageName = sImageName;
    }
    
    public String getImagePath()
    {
        return imagePath;
    }
    
    public void setImagePath(String sImagePath)
    {
        imagePath = sImagePath;
    }
    
    public String getCaption()
    {
        return caption;
    }
    
    public void setCaption(String sCaption)
    {
        caption = sCaption;
    }
    
    public void setWidth(double wNum)
    {
        width = wNum;
    }
    
    public double getWidth()
    {
        return width;
    }
    
    public void setHeight(double hNum)
    {
        width = hNum;
    }
    
    public double getHeight()
    {
        return height;
    }
    
    public File getImageFile()
    {
        return imageFile;
    }
    
    public void setImageFIle(File file)
    {
        imageFile = file;
    }
            
    
}
