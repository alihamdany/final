/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Ali
 */
public class List {
    
    private ObservableList<String> list;
    
    
    public List()
    {
        list= FXCollections.observableArrayList();
    }
    
    public  void addItem(String sItem)
    {
        list.add(sItem);
    }
    
    public  void removeItem(String sItem)
    {
        list.remove(sItem);
    }
    
    public ObservableList<String> getList()
    {
        return list;
    }
  
    
    


}
