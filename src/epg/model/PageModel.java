 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import epg.view.MainUI;
import java.net.MalformedURLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ssm.model.Slide;

/**
 *
 * @author Ali
 */
public class PageModel {
    
     ObservableList<Page> pages;
     MainUI ui;
     Page selectedPage;
     String title;
     
     public PageModel(MainUI ui)
     {
         this.ui = ui;
         pages = FXCollections.observableArrayList();
         title = "untitled";
         //reset();
     }
     
      // ACCESSOR METHODS
    public boolean isPageSelected() {
	return selectedPage != null;
    }
    
    public boolean isSelectedPage(Page testPage) {
	return selectedPage == testPage;
    }
    
    public ObservableList<Page> getPages() {
	return pages;
    }
    
    public Page getSelectedPage() {
	return selectedPage;
    }

    public String getTitle() { 
	return title; 
    }
    
    // MUTATOR METHODS
    public void setSelectedPage(Page initSelectedPage) {
	selectedPage = initSelectedPage;
    }
    
    public void setTitle(String initTitle) { 
	title = initTitle; 
        //selectedPage.setTitle(title);
    }

    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
	pages.clear();
	title = "Untitled";
	selectedPage = null;
    }

    /**
     * Adds a slide to the slide show with the parameter settings.
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     * @param initCaption Caption for the slide image to add.
     */
    public void addPage() throws MalformedURLException {
	Page pageToAdd = new Page();
	pages.add(pageToAdd);
        System.out.println(pages.size());
        ui.reloadPage();
	//ui.reloadSlideShowPane();
    }

    /**
     * Removes the currently selected slide from the slide show and
     * updates the display.
     */
    public void removeSelectedPage() {
	if (isPageSelected()) {
	    pages.remove(selectedPage);
	    selectedPage = null;
	    //ui.reloadSlideShowPane();
	}
    }
 
    /**
     * Moves the currently selected slide up in the slide
     * show by one slide.
     */
    public void moveSelectedPAgeUp() {
	if (isPageSelected()) {
	    movePageUp(selectedPage);
	    //ui.reloadSlideShowPane();
	}
    }
    
    // HELPER METHOD
    private void movePageUp(Page pageToMove) {
	int index = pages.indexOf(pageToMove);
	if (index > 0) {
	    Page temp = pages.get(index);
	    pages.set(index, pages.get(index-1));
	    pages.set(index-1, temp);
	}
    }
    
    /**
     * Moves the currently selected slide down in the slide
     * show by one slide.
     */
    public void moveSelectedPageDown() {
	if (isPageSelected()) {
	    movePageDown(selectedPage);
	   // ui.reloadSlideShowPane();
	}
    }
    
    // HELPER METHOD
    private void movePageDown(Page pageToMove) {
	int index = pages.indexOf(pageToMove);
	if (index < (pages.size()-1)) {
	    Page temp = pages.get(index);
	    pages.set(index, pages.get(index+1));
	    pages.set(index+1, temp);
	}
    }
    
    /**
     * Changes the currently selected slide to the previous slide
     * in the slide show.
     */
    public void previous() {
	if (selectedPage == null)
	    return;
	else {
	    int index = pages.indexOf(selectedPage);
	    index--;
	    if (index < 0)
		index = pages.size() - 1;
	    selectedPage = pages.get(index);
	}
    }

    /**
     * Changes the currently selected slide to the next slide
     * in the slide show.
     */    
    public void next() {
    	if (selectedPage == null)
	    return;
	else {
	    int index = pages.indexOf(selectedPage);
	    index++;
	    if (index >= pages.size())
		index = 0;
	    selectedPage = pages.get(index);
	}
    }    
    
}
