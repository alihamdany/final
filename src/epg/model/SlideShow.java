/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import ssm.model.SlideShowModel;

/**
 *
 * @author Ali
 */
public class SlideShow {
    
    private SlideShowModel slideshowModel;
    
    public SlideShow(SlideShowModel model)
    {
        slideshowModel = model;
    }
    
    public SlideShowModel getSlideShow()
    {
        return slideshowModel;
    }
    
    public void setSlideShow(SlideShowModel sModel)
    {
        slideshowModel = sModel;
    }
    
}
