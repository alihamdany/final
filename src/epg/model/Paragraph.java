/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

/**
 *
 * @author Ali
 */
public class Paragraph {
    
    String text;
    String hyperlink;
    
    public Paragraph(String pText)
    {
        text = pText;
        //hyperlink = pHyperlink;
    }
    
    public String getParagraphText(){
        return text;
    }
    
    public void setParagraphText(String sText ){
        text = sText;
    }
    
    public String getHyperlink(){
        return hyperlink;
    }
    
    public void setHyperlink(String sHyperlink ){
        hyperlink = sHyperlink;
    }
    
}
