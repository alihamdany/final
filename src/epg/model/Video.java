/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

/**
 *
 * @author Ali
 */
public class Video {
    
    String videoName;
    String videoPath;
    String caption;
    double width;
    double height;
    
    public Video(String fileName, String filePath, String cap, double wid, double hei ){
            videoName = fileName;
            videoPath = filePath;
            caption = cap;
            width = wid;
            height = hei;
        }
    
    public String getVideoName()
    {
        return videoName;
    }
    
    public void setVideoName(String sVideoName)
    {
        videoName = sVideoName;
    }
    
    public String getVideoPath()
    {
        return videoPath;
    }
    
    public void setVideoPath(String sVideoPath)
    {
        videoPath = sVideoPath;
    }
    
    public String getCaption()
    {
        return caption;
    }
    
    public void setCaption(String sCaption)
    {
        caption = sCaption;
    }
    
    public void setWidth(double wNum)
    {
        width = wNum;
    }
    
    public double getWidth()
    {
        return width;
    }
    
    public void setHeight(double hNum)
    {
        width = hNum;
    }
    
    public double getHeight()
    {
        return height;
    }
    
    
}
