/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.controller;

import epg.view.HeaderEditView;
import epg.view.ImageEditView;
import epg.view.ListEditView;
import epg.view.MainUI;
import epg.view.PageUI;
import epg.view.ParagraphEditView;
import epg.view.SlideshowEditView;
import epg.view.VideoEditView;
import java.net.MalformedURLException;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import ssm.file.SlideShowFileManager;
import ssm.view.SlideShowMakerView;

/**
 *
 * @author Ali
 */
public class ComponentController {
    private MainUI ui;
    private PageUI pageui;
    //HeaderEditView hev;
    
    public ComponentController(MainUI theui, PageUI pageui){
        
        ui = theui;
        this.pageui = pageui;
        
}
    
    public void handleAddRequest(String component) throws MalformedURLException{
            
       
        if(component.equals("header")){
             new HeaderEditView(pageui,ui).initWindow();
            }
        
        else if(component.equals("paragraph"))
            new ParagraphEditView(pageui,ui,"").initWindow();
        else if(component.equals("list"))
            new ListEditView(pageui,ui).initWIndow();
        else if(component.equals("image")){
            ImageEditView imageEditView = new ImageEditView(ui,pageui);
            imageEditView.initWindow();
        }
        else if(component.equals("video"))
            new VideoEditView();
        else if(component.equals("slideShow"))
                new SlideShowMakerView(new SlideShowFileManager()).startUI(new Stage(), "Edit Slideshow");
        else{}
        
        
        
        }
    
    public void handleRemoveRequest(String component, HBox container){
        
        container.getChildren().clear();
        
    }
    
    public void handleEditHeaderRequest(Text text)
    {
       //HeaderEditView hev= new HeaderEditView(pageui,ui);
       //hev.editTextField(text);
       //hev.initWindow();
    }
    
    public void handleEditParagraphRequest(Text text)
    {
       ParagraphEditView pev= new ParagraphEditView(pageui,ui,"edit");
       pev.editTextField(text);
       pev.initWindow();
    }
    
}
