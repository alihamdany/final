/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg;

/**
 *
 * @author Ali
 */
public class StartupConstants {
    public static String ENGLISH_LANG = "English";
    public static String FINNISH_LANG = "Finnish";

    // WE'LL LOAD ALL THE UI AND LANGUAGE PROPERTIES FROM FILES,
    // BUT WE'LL NEED THESE VALUES TO START THE PROCESS

    public static String PROPERTY_TYPES_LIST = "property_types.txt";
    public static String UI_PROPERTIES_FILE_NAME_English = "properties_EN.xml";
    public static String UI_PROPERTIES_FILE_NAME_Finnish = "properties_FI.xml";
    public static String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";
    public static String PATH_DATA = "./data/";
    public static String PATH_PAGES = PATH_DATA + "pages/";
    public static String PATH_IMAGES = "./images/";
    public static String PATH_ICONS = PATH_IMAGES + "icons/";
    public static String PATH_IMG = PATH_IMAGES + "img/";
    public static String PATH_SLIDE_SHOW_IMAGES = PATH_IMAGES + "slide_show_images/";
    public static String PATH_CSS = "/epg/style/";
    public static String STYLE_SHEET_UI = PATH_CSS + "MainUIStyle.css";
    public static String STYLE_SHEET_UIHEADER = PATH_CSS + "headerStyle.css";

    // HERE ARE THE LANGUAGE INDEPENDENT GUI ICONS
    public static String ICON_WINDOW_LOGO = "mainWindow.png";
    public static String ICON_NEW_EPORTFOLIO = "New.png";
    public static String ICON_LOAD_EPORTFOLIO = "Load.png";
    public static String ICON_SAVE_EPORTFOLIO = "Save.png";
    public static String ICON_SAVEAS_EPORTFOLIO = "SaveAs.png";
    public static String ICON_EXPORT_EPORTFOLIO = "Export.png";
    public static String ICON_EXIT = "Exit.png";
    public static String LIVE_VIEW = "Exit.png";
    public static String EDIT_VIEW = "Exit.png";
    public static String ICON_ADD_PAGE = "AddPage.png";
    public static String ICON_REMOVE_PAGE = "Remove.png";
    public static String ICON_MOVE_UP = "MoveUp.png";
    public static String ICON_MOVE_DOWN = "MoveDown.png";
    public static String ICON_PREVIOUS = "Previous.png";
    public static String ICON_NEXT = "Next.png";
    public static String ICON_VIEW = "View.png";
    public static String ICON_EDIT = "EditView.png";
    public static String ICON_ADD = "Add.png";
    public static String ICON_REMOVE = "Remove.png";
    public static String ICON_INSERT_PHOTO = "InsertPhoto.png";    
    public static String ICON_INSERT_VIDEO = "AddMovie.png";
    public static String ICON_INSERT_SLIDESHOW = "AddSlideshow.png";
    public static String ICON_INSERT_LIST = "List.png";
    public static String ICON_INSERT_PARAGRAPH = "Paragraph.png";
    public static String ICON_CHANGE_FONT = "Font.png";
    public static String ICON_CHANGE_COLOR = "Color.png";
    public static String ICON_OK = "OK.png";
    public static String ICON_AddList = "AddList.png";
    public static String ICON_UP = "UP.png";
    public static String ICON_DOWN = "DOWN.png";
    public static String ICON_CHANGE_LAYOUT = "layout.png";


    // UI SETTINGS
    public static String    DEFAULT_SLIDE_IMAGE = "DefaultStartSlide.png";
    public static int	    DEFAULT_THUMBNAIL_WIDTH = 200;
    public static int	    DEFAULT_SLIDE_SHOW_HEIGHT = 500;
    
    // CSS STYLE SHEET CLASSES
    public static String    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON = "vertical_toolbar_button";
    public static String    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON = "horizontal_toolbar_button";
    public static String    CSS_CLASS_SLIDE_SHOW_EDIT_VBOX = "slide_show_edit_vbox";
    public static String    CSS_CLASS_SLIDE_EDIT_VIEW = "slide_edit_view";
    public static String    CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW = "selected_slide_edit_view";
    
    // UI LABELS
    public static String    LABEL_SLIDE_SHOW_TITLE = "slide_show_title";
    public static String    LABEL_LANGUAGE_SELECTION_PROMPT = "Select a Language:";
    public static String    OK_BUTTON_TEXT = "OK";
    
    //BUTTON TOOLTIPS
    public static String    TOOLTIP_NEW_EPORTFOLIO = "New";
    public static String    TOOLTIP_LOAD_EPORTFOLIO= "Load";
    public static String    TOOLTIP_SAVE_EPORTFOLIO = "Save";
    public static String    TOOLTIP_SAVEAS_EPORTFOLIO = "Save As";
    public static String    TOOLTIP_EXPORT_EPORTFOLIO= "Export";
    public static String    TOOLTIP_EXIT = "Exit";
    
}

